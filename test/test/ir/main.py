from datetime import datetime, timedelta, timezone
import time, json, requests, cv2, Adafruit_ADS1x15, os
DEBUG = True

config = None

if __name__ == "__main__":
    with open('config') as data_file:
        config = json.load(data_file)
    print(config['server'])
    print(config['port'])
    GAIN = 1
    adc = Adafruit_ADS1x15.ADS1115()
    cap = cv2.VideoCapture(0)
    state, img = cap.read()
    if not state :
        print("Camera Error catch!")
        os.system("uv4l --driver raspicam --auto-video_nr --width 640 --height 480 --encoding jpeg --frame-time 0")
        cap = cv2.VideoCapture(0)
        print("Camera Error fix")
    img = None
    state = None

    print("START!!!")
    while True :
        values = [[], []]
        for i in range(0, 100):
            values[0].append(adc.read_adc(0, gain=GAIN))
            values[1].append(adc.read_adc(1, gain=GAIN))

        max0 = max(values[0])
        min0 = min(values[0])
        max1 = max(values[1])
        min1 = min(values[1])
        if DEBUG :
            print("CH.0 MAX : " + str(max(values[0])))
            print("CH.0 MIN : " + str(min(values[0])))
            print("CH.1 MAX : " + str(max(values[1])))
            print("CH.1 MIN : " + str(min(values[1])))
            print("-" * 20)
            print()

        if(max0 > 9000 or min0 < 1000 or min1 < 1800 or max1 > 7650):
            print("DETECT WATER!!!")
        else:
            print("AIR?")
            now = datetime.now()
            cap_time = str(datetime.time(now)).replace(' ', '_') + '.jpg'
            _, img = cap.read()
            cv2.imwrite(cap_time, img)
            files = {'files': (cap_time, open(cap_time, 'rb'), 'application/*', {})}
            r = requests.post('http://' + config['server'] + ':' + str(config['port']), files=files, data={ 'type': 'original', 'timestamp':time.mktime(now.timetuple())})
            print(r.text)
            print("TIME : " + str(datetime.now() - now))

