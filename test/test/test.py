import cv2
import numpy as np
import copy, requests
import os, datetime, time, sys
import _thread, base64, json
import RPi.GPIO as GPIO


from matplotlib import pyplot as plt
from datetime import datetime, timedelta, timezone


timeout = 0
LED_PIN = 4
# 100px = 0.3mm
config = None 
template = None
end_template = None
cap = None
size_x = None
cap_time = None
detect_time = None

def send_image(detect, original):
	global cap_time
	
	files = {'files': (detect, open(detect, 'rb'), 'application/*', {})}
	r = requests.post('http://hongmu.wiki:3000/', files=files, data={'type': 'detect', 'timestamp':time.mktime(cap_time.timetuple())})
	print(r.text)
	files = {'files': (original, open(original, 'rb'), 'application/*', {})}
	r = requests.post('http://hongmu.wiki:3000/', files=files, data={'type': 'detect', 'timestamp':time.mktime(cap_time.timetuple())})
	print(r.text)

	
def detect_bubble():
	global cap_time
	now = str(datetime.time(cap_time)).replace(' ', '_')
	detect = now + '_detect.jpg';
	original = now + '_original.jpg'
	os.system('cp res.jpg ' + detect)
	os.system('cp test_original.jpg ' + original)
	print("detect img save!!!")
	send_image(detect, original)

def led_thread(LED_PIN, TRASH):
	global timeout
	print('LED PIN : ' + str(LED_PIN))
	print('timeout : ' + str(timeout))
	while True :
		time.sleep(0.5)
		if timeout > 0 :
			timeout = timeout - 1
			GPIO.output(LED_PIN, True)
			print('led high')
		else :
			GPIO.output(LED_PIN, False)
			print('led low')


def pattern(img_rgb):
	global end_template
	global template

	bubble_list = []
	#img_rgb = cv2.imread('test_resultw.jpg')
	img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
	w, h = template.shape[::-1]
	we, he = end_template.shape[::-1]

	res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)
	res_end = cv2.matchTemplate(img_gray, end_template, cv2.TM_CCOEFF_NORMED)
	threshold = 0.5
	threshold_end = 0.6	
	loc = np.where( res >= threshold)
	loc_end = np.where(res_end >= threshold_end)
	before = (0,0)
	temp_zip = zip(*loc_end[::-1])
	for pt in temp_zip:
		if pt[0] - before[0] > 20 :
			bubble_list.append(pt + ('end', ))
			cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0,255,0), 2)
		before = pt

	before = (0,0)
	for pt in zip(*loc[::-1]):
		if pt[0] - before[0] > 20:
			bubble_list.append(pt + ('start', ))
			cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)
		before = pt
	cv2.imwrite('res.jpg',img_rgb)
	bubble_list = sorted(bubble_list, key=lambda bubble: bubble[0])
	print("SAVE")
	return bubble_list

def bubble_fillter(bubble_list):
	result = []
	last_append = None
	before = (0, 0)
	for pt in bubble_list :
		if pt[0] - before[0] > 30 or pt[2] != last_append :
			print('row : ' + str(pt))
			if last_append != pt[2]:
				result.append(pt)
				last_append = pt[2]
		before = pt
	distance = detect_distance(result)
	if distance == None :
		return None
	print("=====DISTANCE=====")
	for dis in distance :
		print(str(dis))
	draw_distance(distance)
	return result

def detect_distance(bubble_list):
	result = []
	global size_x
	back = 't'
	'''거품별 거리를 측정하는 함수'''
	for idx in range(0, len(bubble_list)):
		print(idx)
		if back == bubble_list[idx][2] :
			return None
		back = bubble_list[idx][2]
		if idx < len(bubble_list) :
			print('in')
			if bubble_list[idx][2] == 'end':
				if idx == 0:
					dis = bubble_list[idx][0] / 100 * 0.3
					start = (0, bubble_list[idx][1])
					end = (bubble_list[idx][0], bubble_list[idx][1])
					result.append((dis, start, end))
					print("DIFF : " + str(bubble_list[idx][0]))
			else :
				if idx == len(bubble_list) - 1:
					dis = (size_x - bubble_list[idx][0]) / 100 * 0.3
					start = (bubble_list[idx][0], bubble_list[idx][1])
					end = (size_x, bubble_list[idx][1])
					result.append((dis, start, end))
					print("DIFF : " + str(size_x - bubble_list[idx][0]))
				else :
					dis = (bubble_list[idx + 1][0] - bubble_list[idx][0]) / 100 * 0.3
					start = (bubble_list[idx][0], bubble_list[idx][1])
					end =  (bubble_list[idx + 1][0], bubble_list[idx + 1][1])
					result.append((dis, start, end))
					print("DIFF : " + str((bubble_list[idx + 1][0] - bubble_list[idx][0]) / 100 * 0.3))

	return result

def draw_distance(distance_list):
	global timeout
	img = cv2.imread('res.jpg', 1)
	catch = False
	for distance in distance_list:
		x = int((distance[2][0] + distance[1][0]) / 2)
		y = int((distance[2][1] + distance[1][1]) / 2)
		dis = '{:.3f}'.format(distance[0])+"cm"
		print(dis)
		cv2.line(img, distance[1], distance[2], (0,255,255), 1)
		cv2.putText(img, dis, (x, y), cv2.FONT_HERSHEY_SIMPLEX,  1, (0, 255, 255), thickness=1)
		catch = True
	cv2.imwrite('res.jpg', img)
	if catch == True :
		timeout = 3
		detect_bubble()


def calcu_lux(y):
	temp = []
	for row in y:
		temp.append(np.mean(row))
	return np.mean(temp)

def check_camera() :
	global cap
	call = os.system
	call("sudo modprobe bcm2835-v4l2")

	print("test1")
	if os.path.exists("/dev/video0"):
		call("wget http://www.linux-projects.org/listing/uv4l_repo/lrkey.asc && sudo apt-key add ./lrkey.asc")
		print("test2")
		call("cp ./source_list /etc/apt/sources.list")
		print("test3")
		call("sudo apt-get update")
		print("test4")
		call("sudo apt-get install uv4l uv4l-raspicam")
		print("test5")
		call("uv4l --driver raspicam --auto-video_nr --width 640 --height 480 --encoding jpeg --frame-time 0")
		print("test6")
		call("dd if=/dev/video0 of=snapshot.jpeg bs=11M count=1")
		print("test7")
		if os.path.exists("./snapshot.jpeg"):
			print("[SYSTEM] Now UV4L device driver operated...")
			call("rm lrkey*")
		else :
			print("[SYSTEM] Fail...")
			exit()

def test():
	global cap_time
	global detect_time
	global size_x
	global cap
	while cap.isOpened():
		_, img = cap.read()
		cap_time = datetime.now()
		img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		#img = cv2.imread('test1.jpg',0)
		#img = cv2.resize(img, (0,0), fx=0.3, fy=0.3)
		cv2.imwrite('test_original.jpg', img)
		height, size_x = img.shape[:2]
		original_img = cv2.imread('test_original.jpg', 1)
		yuv_img = cv2.cvtColor(original_img, cv2.COLOR_BGR2YUV)
		y, u, v = cv2.split(yuv_img)
		img_lux = calcu_lux(y)
		print('lux : ' + str(img_lux))
		if img_lux > 60 :
			laplacian = cv2.Laplacian(img,cv2.CV_64F)
			sobelx = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=5)
			#sobely = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=5)
			cv2.imwrite('test_result64.jpg', laplacian)
			cv2.imwrite('test_resultx.jpg', sobelx)
			#ret, thr = cv2.threshold(sobelx, 200, 255, cv2.THRESH_BINARY)
			#cv2.imwrite('test_resultw.jpg', thr)
			#ret, thr = cv2.threshold(sobely, 200, 255, cv2.THRESH_BINARY)
			#cv2.imwrite('test_resulty.jpg', sobely)
			thr = cv2.imread('test_resultx.jpg', 1)
			bubbles = pattern(thr)
			print("Pattern : " + str(bubbles))
			bubbles = bubble_fillter(bubbles)
			detect_time = datetime.now()
			print('총 걸린 시간 : ' + str(detect_time - cap_time))
			if bubbles != None :
				for pt in bubbles:
					print(str(pt[0]) + '\t' + str(pt[1]) + '\t' + pt[2])
				time.sleep(1)
		else :
			'''빛이 적을때의 처리'''
			print("LUX....")

if __name__ == "__main__":
	global cap
	global template
	global end_template
	global config
	print("START")
	try:
		with open('config') as data_file:    
			config = json.load(data_file)
		print(config['server'])
		print(config['port'])
		template = cv2.imread('pattern.png',0)
		end_template = cv2.imread('y_pattern.png', 0)
		GPIO.setmode(GPIO.BCM)
		GPIO.setup(LED_PIN, GPIO.OUT)
		#check_camera()
		cap = cv2.VideoCapture(0)
		_thread.start_new_thread(led_thread, (LED_PIN, 'house'))
		test()
	except Exception as err:
		print(str(err))
	finally :
		GPIO.cleanup()

